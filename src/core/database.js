import Sequelize from 'sequelize';
import dbConfig from '../config/database';
import modelList from './model-list';


class Database {

    constructor(){
        this.init()
    }

    init() {
        this.connection = new Sequelize(dbConfig);
        modelList
            .map(model=>model.init(this.connection));
    }

}

export default new Database();
