import helloRoutes from '../app/modules/Hello/routes/routes';
import userRoutes from '../app/modules/User/routes/routes';

const routes  = [
    helloRoutes,
    userRoutes,
]

export default routes;