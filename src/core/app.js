import 'dotenv/config';
import express  from 'express';
import routes from './routes';
import path from 'path';

import './database';


class App {
    constructor(){
        this.server = express();
        this.init()
    }

    init(){
        console.log(`Application is Running on port ${process.env.APP_PORT_LISTEN} `)
        this.middlewares()
        this.routes()
    }

    routes(){
        for( let route of routes ){
            this.server.use(route)
        }
    }

    middlewares(){
        this.server.use(express.json());
        this.server.use('/files',express.static(path.resolve('..','..','tmp', 'uploads')));
    }
}

export default new App().server;