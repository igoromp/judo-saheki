import {Router} from 'express';
import UserController from '../controller/user-controller';

const routes = new Router();

routes.get('/users', UserController.list);
routes.post('/users/add', UserController.store);

export default routes;
