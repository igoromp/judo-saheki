import httpStatus from '../../Base/utils/http-status';
import Usuarios from '../model/usuario-model';

class UserController {

    async list(req, res) {
        return res.json({users:{}});
    }

    async store(req, res) {
       try {
            const {usuario:user} = req.body;
            
            if(!user){
                return res.json(httpStatus.BAD_REQUEST(
                    'Data inconsistency',
                    'Non-existent or inconsistent data',
                ))
            }

            if (await Usuarios.findOne({where:{email:user.email}})) {
                return res.json(httpStatus.BAD_REQUEST(
                    'Duplication of registration',
                    'User alredy exists',
                ))
            }

            let {
                nome,
                idade,
                turno, 
                turma,
                email, 
                faixa,
                status:ativo,

            } = await  Usuarios.create(user);
            

            return res.json({dados:{
                nome,
                idade,
                turno, 
                turma,
                email, 
                faixa,
                status:ativo,
                
            }});

       } catch(e) {
            return res.json(httpStatus.INTERNAL_SERVER_ERROR(e.name, e.message))
       }
    }   

}

export default new UserController();