import Sequilize, {Model, Sequelize} from 'sequelize';
import bcrypt from 'bcryptjs';

class Usuarios extends Model {
    static init(sequelize) {
        super.init({
            nome:Sequilize.STRING,
            idade:Sequilize.INTEGER,
            faixa: Sequilize.INTEGER,
            turno: Sequilize.STRING,
            turma: Sequilize.STRING,
            email: Sequilize.STRING,
            password:Sequilize.VIRTUAL,
            password_hash:Sequilize.STRING,
            telefone:Sequelize.STRING
        }, 
        {
            sequelize,
        });

        this.addHook('beforeSave', async user=>{
            if(user.password){
                user.password_hash = await bcrypt.hash(user.password, 8)
            }
        });

        return this;
    }

    checkPassword(password) {
        return bcrypt.compare(password, this.password_hash);
    }
}

export default Usuarios;