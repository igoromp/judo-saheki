export default {
    OK:(obj)=>{
        return {
            obj,
            status:200
        }
    },
    CREATED:201,
    NO_CONTENT:204,
    BAD_REQUEST:(title,details)=>{
        return {
           error:{
            title,
            details,
            status:400
           }
        }
    },
    UNAUTHORIZED:(title,details)=>{
        return {
           error:{
            title,
            details,
            status:401
           }
        }
    },

    INTERNAL_SERVER_ERROR:(title,details)=>{
        return {
           error:{
            title,
            details,
            status:500
           }
        }
    },
};