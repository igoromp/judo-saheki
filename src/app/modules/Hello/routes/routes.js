import HelloController from '../controller/hello-ctrl';
import { Router } from 'express';

const routes = new Router();

routes.get('/', HelloController.init);

export default routes;