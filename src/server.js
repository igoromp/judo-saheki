import 'dotenv/config';
import App from './core/app'

App.listen(process.env.APP_PORT_LISTEN).setTimeout(parseInt(process.env.APP_TIMEOUT));

