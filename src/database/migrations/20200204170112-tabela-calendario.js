'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('calendario',{
        id:{type:Sequelize.INTEGER, allowNull:false, autoIncrement: true, primaryKey:true},
        titulo:{type:Sequelize.STRING, allowNull:false},
        conteudo:{type:Sequelize.TEXT, allowNull:false},
        criador:{type:Sequelize.INTEGER, references:{model:'usuarios',key: 'id'},onDelete:'CASCADE'},
        created_at: {type: Sequelize.DATE,allowNull: false},
        updated_at: {type: Sequelize.DATE,allowNull: false},
      })
  },

  down: queryInterface => {
      return queryInterface.dropTable('calendario')
  }
};
