'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('desempenho', 'exercicio_id', {
      type: Sequelize.INTEGER,
      reference: { model: 'exercicios', key: 'id' },
      onDelete: 'SET NULL',
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('desempenho', 'exercicio_id');
  },
};
