'use strict';

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.addColumn('usuarios', 'faixa', {
      type: Sequelize.INTEGER,
      reference: { model: 'config', key: 'code' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    });

    await queryInterface.addColumn('responsavel_usuario', 'grau_parentesco',{
      type: Sequelize.INTEGER,
      reference: { model: 'config', key: 'code' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('usuarios', 'faixa');
  },
};
