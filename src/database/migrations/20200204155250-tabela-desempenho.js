'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('desempenho',{
        id:{type:Sequelize.INTEGER,allowNull:false, autoIncrement:true, primaryKey:true},
        aluno_id:{type:Sequelize.INTEGER, references:{model:'usuarios', key:'id'}, allowNull:false, onDelete:'CASCADE'},
        quantidade:{type:Sequelize.INTEGER, allowNull:false},
        tempo_duracao:{type:Sequelize.INTEGER, allowNull:false},
        data_hora:{type:Sequelize.DATE, allowNull:false},
      });
  },

  down: queryInterface => {
    return queryInterface.dropTable('desempenho');
  }
};
