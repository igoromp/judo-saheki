'use strict';

module.exports = {
  up: (queryInterface, Sequelize)=>{
    return queryInterface.createTable('usuarios',{
        id:{type:Sequelize.INTEGER, allowNull:false, autoIncrement: true, primaryKey:true},
        nome:{type:Sequelize.STRING, allowNull:false},
        idade:{type: Sequelize.INTEGER, allowNull:true},
        turno:{type:Sequelize.STRING, allowNull:true},
        turma:{type: Sequelize.STRING, allowNull:true},
        email:{type:Sequelize.STRING, allowNull:false, unique:true},
        password_hash:{type:Sequelize.STRING, allowNull:false},
        telefone:{type:Sequelize.STRING, allowNull:true},
        status:{type:Sequelize.BOOLEAN, allowNull:false,defaultValue: true},
        created_at: {type: Sequelize.DATE,allowNull: false},
        updated_at: {type: Sequelize.DATE,allowNull: false},
    });
},
  down: queryInterface => {
      return queryInterface.dropTable('usuarios');
  }
};
