'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('exercicios',{
        id:{type:Sequelize.INTEGER, allowNull:false, autoIncrement:true, primaryKey:true},
        nome:{type:Sequelize.STRING, allowNull: false},
        tipo:{type:Sequelize.INTEGER, allowNull:false},
        dificuldade:{type:Sequelize.INTEGER, allowNull:false}
      });
  },

  down: queryInterface => {
      return queryInterface.dropTable('exercicios');
  }
};
