'use strict';

module.exports = {
  up: (queryInterface, Sequelize)=>{
    return queryInterface.createTable('responsavel_usuario',{
        usuario_id:{type: Sequelize.INTEGER, references:{model:'usuarios', key:'id'}, onDelete:'CASCADE', allowNull:false},
        responsavel_id:{type: Sequelize.INTEGER, references:{model:'responsaveis', key:'id'}, onDelete:'CASCADE', allowNull:false},
        created_at: {type: Sequelize.DATE,allowNull: false},
        updated_at: {type: Sequelize.DATE,allowNull: false},
    });
},
  down: queryInterface=>{
      return queryInterface.dropTable('responsavel_usuario');
  }
};
