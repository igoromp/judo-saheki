'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('config',{
        id:{type:Sequelize.INTEGER, allowNull:false, autoIncrement: true, primaryKey:true},
        code:{type:Sequelize.INTEGER, allowNull:false, unique:true},
        desc:{type:Sequelize.STRING, allowNull:false},
        created_at: {type: Sequelize.DATE,allowNull: false},
        updated_at: {type: Sequelize.DATE,allowNull: false},
      })
  },

  down: queryInterface => {
      return queryInterface.dropTable('config')
  }
};

